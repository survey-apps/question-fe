#!/usr/bin/env bash

# This script is used to merge the develop branch into the main branch.

develop_branch="develop"
master_branch="main"

# Check if the current branch is the develop branch
git switch $master_branch

# Pull the latest changes from the remote develop branch
git pull origin $master_branch

# Merge the develop branch into the main branch
git merge $develop_branch
if [ $? -ne 0 ]; then
    echo -e "\033[31m\nFailed to merge the develop branch into the main branch.\n\033[0m"
    exit 1
fi

# Push the changes to the remote main branch
git push origin $master_branch

# Switch back to the develop branch
git switch $develop_branch

# Print success message
echo -e "\033[32m\nSuccessfully merged the develop branch into the main branch.\n\033[0m"

exit 0
#EOF
