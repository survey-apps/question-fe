import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation, useSearchParams } from 'react-router-dom'
import { Input } from 'antd'
import { LIST_SEARCH_PARAM_KEY } from '@/constants'

const { Search } = Input

const ListSearch: React.FC = () => {
  const navigate = useNavigate()
  const { pathname } = useLocation()

  const [value, setValue] = useState<string>('')
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  // 获取 url 参数，并设置到 value
  const [searchParams] = useSearchParams()
  const [uriParams, setUriParams] = useState<string[]>([])

  useEffect(() => {
    const currentValue = searchParams.get(LIST_SEARCH_PARAM_KEY) || ''
    setValue(currentValue)

    const currentUriParams: string[] = []
    searchParams.forEach((val, key) => {
      currentUriParams.push(`${key}=${val}`)
    })
    setUriParams(currentUriParams)
  }, [searchParams])

  // 搜索内容以查询参数形式存储
  const handleSearch = (value: string) => {
    uriParams.length > 0 &&
      uriParams.forEach(element => {
        if (element.startsWith(`${LIST_SEARCH_PARAM_KEY}=`)) {
          // 删除原来的值
          uriParams.splice(uriParams.indexOf(element), 1)
        }
      })
    value && uriParams.push(`${LIST_SEARCH_PARAM_KEY}=${value}`)
    navigate({
      pathname,
      search: uriParams.join('&')
    })
  }

  return (
    <Search
      size="large"
      allowClear
      placeholder="请输入关键字"
      value={value}
      onChange={handleChange}
      onSearch={handleSearch}
      style={{ width: '260px' }}
    />
  )
}

export default ListSearch
