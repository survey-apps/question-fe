import React from 'react'
import { useNavigate, Link } from 'react-router-dom'
import { Button, Space, Divider, Tag, Popconfirm, Modal, message } from 'antd'
import {
  EditOutlined,
  LineChartOutlined,
  StarOutlined,
  CopyOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined
} from '@ant-design/icons'
import { POST_EDIT_PATHNAME, POST_STATISTIC_PATHNAME } from '@/router/paths'
import { QuestionCardProps } from './types'
import styles from './styles.module.scss'

const { confirm } = Modal

const QuestionCard: React.FC<QuestionCardProps> = props => {
  const navigate = useNavigate()
  const { _id, title, isPublished, isStar, answerCount, createdAt } = props

  const handleDuplicate = () => {
    message.success('复制成功！')
  }
  const handleDelete = () => {
    confirm({
      title: '确定删除该问卷？',
      icon: <ExclamationCircleOutlined />,
      onOk: () => message.info('删除成功！')
    })
  }

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        <div className={styles.left}>
          <Link
            to={isPublished ? `${POST_STATISTIC_PATHNAME}/${_id}` : `${POST_EDIT_PATHNAME}/${_id}`}
          >
            <Space>
              {isStar && <StarOutlined style={{ color: 'red' }} />}
              {title}
            </Space>
          </Link>
        </div>
        <div className={styles.right}>
          <Space>
            {isPublished ? <Tag color="processing">已发布</Tag> : <Tag>未发布</Tag>}
            <span>答卷：{answerCount}</span>
            <span>{createdAt}</span>
          </Space>
        </div>
      </div>
      <Divider className={styles.divider} />
      <div className={styles['button-container']}>
        <div className={styles.left}>
          <Space>
            <Button
              icon={<EditOutlined />}
              type="text"
              size="small"
              onClick={() => navigate(`${POST_EDIT_PATHNAME}/${_id}`)}
            >
              编辑问卷
            </Button>
            <Button
              icon={<LineChartOutlined />}
              type="text"
              size="small"
              onClick={() => navigate(`${POST_STATISTIC_PATHNAME}/${_id}`)}
              disabled={!isPublished}
            >
              数据统计
            </Button>
          </Space>
        </div>
        <div className={styles.right}>
          <Space>
            <Button icon={<StarOutlined />} type="text" size="small">
              {isStar ? '取消标星' : '标星'}
            </Button>
            <Popconfirm
              title="是否复制该问卷？"
              okText="确定"
              cancelText="取消"
              onConfirm={handleDuplicate}
            >
              <Button icon={<CopyOutlined />} type="text" size="small">
                复制
              </Button>
            </Popconfirm>

            <Button icon={<DeleteOutlined />} type="text" size="small" onClick={handleDelete}>
              删除
            </Button>
          </Space>
        </div>
      </div>
    </div>
  )
}

export default QuestionCard
