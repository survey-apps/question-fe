import React, { useState } from 'react'
import { Typography } from 'antd'
import ListSearch from './components/ListSearch'
import QuestionCard from './components/QuestionCard'
import styles from './styles.module.scss'
import useTitle from '@/hooks/useTitle'
import type { QuestionCardProps } from './components/QuestionCard/types'

const { Title } = Typography

const rawQuestionList = [
  {
    _id: 'q1',
    title: '问卷1',
    isPublished: true,
    isStar: false,
    answerCount: 5,
    createdAt: '5月10日 12:00'
  },
  {
    _id: 'q2',
    title: '问卷2',
    isPublished: true,
    isStar: true,
    answerCount: 4,
    createdAt: '5月11日 14:30'
  },
  {
    _id: 'q3',
    title: '问卷3',
    isPublished: false,
    isStar: false,
    answerCount: 6,
    createdAt: '5月12日 15:55'
  },
  {
    _id: 'q4',
    title: '问卷4',
    isPublished: true,
    isStar: false,
    answerCount: 8,
    createdAt: '5月13日 17:30'
  }
]

const Lists: React.FC = () => {
  useTitle('Lists')
  const [questionList] = useState<QuestionCardProps[]>(rawQuestionList)

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.left}>
          <Title level={3}>我的问卷</Title>
        </div>
        <div className={styles.right}>
          <ListSearch />
        </div>
      </div>
      <div className={styles.content}>
        {questionList.length > 0 &&
          questionList.map(question => {
            const { _id } = question
            return <QuestionCard key={_id} {...question} />
          })}
      </div>
      <div className={styles.footer}>loadMore... 上划加载更多...</div>
    </div>
  )
}

export default Lists
