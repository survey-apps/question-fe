import React, { useState } from 'react'
import { Typography, Empty, Table, Tag, Button, Space, Modal, message } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import ListSearch from './components/ListSearch'
import styles from './styles.module.scss'
import useTitle from '@/hooks/useTitle'
import type { QuestionCardProps } from './components/QuestionCard/types'

const { Title } = Typography
const { confirm } = Modal

const rawQuestionList = [
  {
    _id: 'q1',
    title: '问卷1',
    isPublished: true,
    isStar: false,
    answerCount: 5,
    createdAt: '5月10日 12:00'
  },
  {
    _id: 'q2',
    title: '问卷2',
    isPublished: true,
    isStar: true,
    answerCount: 4,
    createdAt: '5月11日 14:30'
  },
  {
    _id: 'q3',
    title: '问卷3',
    isPublished: false,
    isStar: false,
    answerCount: 6,
    createdAt: '5月12日 15:55'
  }
]

const Trashes: React.FC = () => {
  useTitle('Trashes')
  const [questionList] = useState<QuestionCardProps[]>(rawQuestionList)
  const [selectedIds, setSelectedIds] = useState<React.Key[]>([])

  const handleDelete = () => {
    confirm({
      title: '确认彻底删除该问卷？',
      icon: <ExclamationCircleOutlined />,
      content: '删除后将无法恢复',
      onOk() {
        message.info('删除 ' + selectedIds.join(', '))
      }
    })
  }

  const tableColumns = [
    {
      title: '问卷标题',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: '是否发布',
      dataIndex: 'isPublished',
      key: 'isPublished',
      render: (isPublished: boolean) => {
        return isPublished ? <Tag color="processing">已发布</Tag> : <Tag>未发布</Tag>
      }
    },
    {
      title: '答卷数',
      dataIndex: 'answerCount',
      key: 'answerCount'
    },
    {
      title: '创建时间',
      dataIndex: 'createdAt',
      key: 'createdAt'
    }
  ]

  const TableElem = (
    <>
      <div style={{ marginBottom: '16px' }}>
        <Space>
          <Button type="primary" disabled={selectedIds.length === 0}>
            恢复
          </Button>
          <Button danger disabled={selectedIds.length === 0} onClick={handleDelete}>
            彻底删除
          </Button>
        </Space>
      </div>
      <Table
        dataSource={questionList}
        columns={tableColumns}
        pagination={false}
        rowKey={question => question._id}
        rowSelection={{
          type: 'checkbox',
          selectedRowKeys: selectedIds,
          onChange: selectedRowKeys => {
            setSelectedIds(selectedRowKeys)
          }
        }}
      />
    </>
  )

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.left}>
          <Title level={3}>回收站</Title>
        </div>
        <div className={styles.right}>
          <ListSearch />
        </div>
      </div>
      <div className={styles.content}>
        {questionList.length === 0 && <Empty description="暂无数据" />}
        {questionList.length > 0 && TableElem}
      </div>
      <div className={styles.footer}>分页</div>
    </div>
  )
}

export default Trashes
