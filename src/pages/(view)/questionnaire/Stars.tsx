import React, { useState } from 'react'
import { Typography, Empty } from 'antd'
import ListSearch from './components/ListSearch'
import QuestionCard from './components/QuestionCard'
import styles from './styles.module.scss'
import useTitle from '@/hooks/useTitle'
import type { QuestionCardProps } from './components/QuestionCard/types'

const { Title } = Typography

const rawQuestionList = [
  {
    _id: 'q1',
    title: '问卷1',
    isPublished: true,
    isStar: true,
    answerCount: 5,
    createdAt: '5月10日 12:00'
  },
  {
    _id: 'q2',
    title: '问卷2',
    isPublished: true,
    isStar: true,
    answerCount: 4,
    createdAt: '5月11日 14:30'
  },
  {
    _id: 'q3',
    title: '问卷3',
    isPublished: false,
    isStar: true,
    answerCount: 6,
    createdAt: '5月12日 15:55'
  }
]

const Stars: React.FC = () => {
  useTitle('Stars')
  const [questionList] = useState<QuestionCardProps[]>(rawQuestionList)

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.left}>
          <Title level={3}>星标问卷</Title>
        </div>
        <div className={styles.right}>
          <ListSearch />
        </div>
      </div>
      <div className={styles.content}>
        {questionList.length === 0 && <Empty description="暂无数据" />}
        {questionList.length > 0 &&
          questionList.map(question => {
            const { _id } = question
            return <QuestionCard key={_id} {...question} />
          })}
      </div>
      <div className={styles.footer}>分页</div>
    </div>
  )
}

export default Stars
