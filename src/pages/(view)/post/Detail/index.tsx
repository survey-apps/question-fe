import { FC } from 'react'
import useTitle from '@/hooks/useTitle'

const View: FC = () => {
  useTitle('Detail')
  return <div>View</div>
}

export default View
