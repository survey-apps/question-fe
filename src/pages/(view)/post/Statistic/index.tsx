import { FC } from 'react'
import useTitle from '@/hooks/useTitle'

const Statistic: FC = () => {
  useTitle('Statistic')
  return <div>Statistic</div>
}

export default Statistic
