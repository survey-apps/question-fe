import { FC } from 'react'
import useTitle from '@/hooks/useTitle'

const Edit: FC = () => {
  useTitle('Edit')
  return <div>Edit</div>
}

export default Edit
