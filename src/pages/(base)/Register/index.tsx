import { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button } from 'antd'
import styles from './styles.module.scss'
import useTitle from '@/hooks/useTitle'

const Register: FC = () => {
  useTitle('Register')
  const navigate = useNavigate()
  return (
    <div className={styles.container}>
      <h1>Register Page</h1>
      <div>
        <Button type="primary" size="large" block onClick={() => navigate(-1)}>
          Return
        </Button>
      </div>
    </div>
  )
}

export default Register
