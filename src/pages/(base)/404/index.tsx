import React from 'react'
import { useNavigate, Link } from 'react-router-dom'
import useTitle from '@/hooks/useTitle'
import styles from './styles.module.scss'
import LottieAnimation from '@/components/LottieAnimation'
import animationError from '@/assets/lotties/error.json'

import SvgPointer from '@/assets/images/pointer.svg?react'
import SvgFacebook from '@/assets/images/facebook.svg?react'
import SvgYoutube from '@/assets/images/youtube.svg?react'
import SvgTwitter from '@/assets/images/twitter.svg?react'
import SvgLinkedin from '@/assets/images/linkedin.svg?react'
import SvgInstagram from '@/assets/images/instagram.svg?react'

const socials = [
  { name: 'Facebook', link: 'https://www.facebook.com/', image: <SvgFacebook /> },
  { name: 'Youtube', link: 'https://www.youtube.com/', image: <SvgYoutube /> },
  { name: 'Twitter', link: 'https://www.twitter.com/', image: <SvgTwitter /> },
  { name: 'Linkedin', link: 'https://www.linkedin.com/', image: <SvgLinkedin /> },
  { name: 'Instagram', link: 'https://www.instagram.com/', image: <SvgInstagram /> }
]

const NotFound: React.FC = () => {
  useTitle('404')

  const navigate = useNavigate()

  return (
    <div className={styles.container}>
      <section className={styles.header}>
        <div className={styles.left}>404 Not Found</div>
        <div className={styles.right}>
          <Link to="#">Home</Link>
          <Link to="#">Portfolio</Link>
          <Link to="#">Contact</Link>
          <Link to="#">Help</Link>
        </div>
      </section>
      <section className={styles.content}>
        <div className={styles.left}>
          <h1 className={styles.title}>Whooops!</h1>
          <div className={styles.description}>
            <p>Sorry, we can't seem to find the page you're looking for.</p>
            <p>Error code 404</p>
          </div>
          <div className={styles.action}>
            <button
              className={styles.btn}
              onClick={() => {
                navigate(-1)
              }}
            >
              Go Back
            </button>
            <SvgPointer className={styles.pointer} />
          </div>
          <div className={styles.footer}>
            <div className={styles.follow}>Follow us</div>
            <div className={styles.social}>
              {socials.map(social => (
                <Link key={social.name} to={social.link} target="_blank" rel="noreferrer">
                  {social.image}
                </Link>
              ))}
            </div>
          </div>
        </div>
        <div className={styles.right}>
          <LottieAnimation
            className={styles.lottie}
            title="404 Not-Found"
            animationData={animationError}
            styleProps={{ width: '1000px', height: '1000px' }}
          />
        </div>
      </section>
    </div>
  )
}

export default NotFound
