import React from 'react'
import styles from './styles.module.scss'
import useTitle from '@/hooks/useTitle'
import Typewriter from '@/components/Typewriter'
import SvgIcon from '@/assets/images/arrow-down.svg?react'

import axios from 'axios'

const Home: React.FC = () => {
  useTitle('Home')

  React.useEffect(() => {
    axios.get('/api/question/1').then(res => {
      console.log(res.data)
    })
  }, [])

  // 新屏幕区域的ref，用来滚动到
  const newScreenRef = React.createRef<HTMLDivElement>()

  // 处理点击事件以滚动到新的屏幕区域
  const scrollToNewScreen = () => {
    if (newScreenRef.current) {
      window.scrollTo({
        top: newScreenRef.current.offsetTop,
        behavior: 'smooth'
      })
    }
  }

  return (
    <>
      <div className={styles.container}>
        <h1 className={styles.title}>Mystic's Blog</h1>
        <Typewriter randomOrder />
        <SvgIcon className={styles.icon} onClick={scrollToNewScreen} />
      </div>
      <div className={styles['new-screen']} ref={newScreenRef}></div>
      <div className={styles['expand-screen1']}></div>
      <div className={styles['expand-screen2']}></div>
      <div className={styles['expand-screen3']}></div>
    </>
  )
}

export default Home
