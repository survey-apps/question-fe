import { useTitle as useAhooksTitle } from 'ahooks'

const useTitle = (title: string) => {
  let domTitle = document.title
  if (domTitle.split(' - ').length > 1) {
    domTitle = domTitle.split(' - ')[0]
  }
  useAhooksTitle(`${domTitle} - ${title}`)
}

export default useTitle
