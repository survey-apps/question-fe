import { NavList } from '@/components/NavBar/types'

export interface HeaderProps {
  className?: string
  title?: string
  navList?: NavList
  triggerHeight?: number
}
