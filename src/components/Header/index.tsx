import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import clsx from 'clsx'
import { Button } from 'antd'
import Logo from '@/components/Logo'
import NavBar from '@/components/NavBar'
import SvgLogo from '@/assets/images/blogger.svg?react'
import styles from './styles.module.scss'
import type { HeaderProps } from './types'
import { LOGIN_PATHNAME, REGISTER_PATHNAME } from '@/router/paths'

const Header: React.FC<HeaderProps> = ({
  className = '',
  title = '',
  navList,
  triggerHeight = 0.7 // 触发高度：第一个屏幕70%的高度
}) => {
  const navigate = useNavigate()

  const [lastScrollTop, setLastScrollTop] = useState(0)
  const [headerClass, setHeaderClass] = useState('')

  useEffect(() => {
    const handleScroll = () => {
      // 获取当前窗口滚动条垂直位置 (保证兼容性)
      const currentScrollTop = window.scrollY || document.documentElement.scrollTop

      // 设置阈值 (滚动距离超过阈值才触发)
      const threshold = window.innerHeight * triggerHeight

      if (currentScrollTop < threshold) {
        // 滚动距离小于阈值时，不触发滑入滑出效果
        setHeaderClass('')
      } else {
        // 滚动距离大于阈值时，触发滑入滑出效果：向下滚动时滑出，向上滚动时滑入
        setHeaderClass(currentScrollTop > lastScrollTop ? styles['slide-out'] : styles['slide-in'])
      }

      // 更新上一次滚动条垂直位置
      setLastScrollTop(currentScrollTop)
    }

    // 添加滚动监听
    window.addEventListener('scroll', handleScroll)

    return () => {
      // 清理滚动监听
      window.removeEventListener('scroll', handleScroll)
    }
  }, [lastScrollTop, triggerHeight])

  return (
    <div className={clsx(styles.header, headerClass, className)}>
      <section className={styles.left}>
        <Logo
          className={styles.logo}
          title={title}
          color="#1772f6"
          SvgIcon={SvgLogo}
          svgWidth={46}
          svgHeight={46}
          showDivider
        />
        <NavBar className={styles.nav} navList={navList} />
      </section>
      <section className={styles.right}>
        <Button type="primary" onClick={() => navigate(LOGIN_PATHNAME)}>
          登录
        </Button>
        <Button type="default" onClick={() => navigate(REGISTER_PATHNAME)}>
          注册
        </Button>
      </section>
    </div>
  )
}

export default Header
