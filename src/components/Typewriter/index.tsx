import React, { useState, useEffect, useRef } from 'react'
import clsx from 'clsx'
import styles from './styles.module.scss'
import type { TypewriterProps } from './types'
import textFrags from './data.json'

const Typewriter: React.FC<TypewriterProps> = ({
  animationTime = 5,
  textFragments = textFrags,
  randomOrder = false
}) => {
  // 文本列表
  const [textList, setTextList] = useState<string[]>([])

  // 转换格式
  useEffect(() => {
    // 随机或顺序排列
    const shuffled = randomOrder ? textFragments.sort(() => Math.random() - 0.5) : textFragments
    const arrays = shuffled.reduce<string[]>((accumulator, currentValue) => {
      const { title, author, contents } = currentValue
      const source = `——— 来自 ${author}《${title}》`
      return [...accumulator, ...contents, source]
    }, [])
    setTextList(arrays)
  }, [textFragments])

  // 当前文本索引
  const [index, setIndex] = useState(0)

  // 切换文本
  useEffect(() => {
    const timer = setInterval(() => {
      setIndex(i => (i + 1) % textList.length)
    }, animationTime * 1000)

    return () => clearInterval(timer)
  }, [textFragments, animationTime, textList.length])

  // 获取当前文本
  const textLine = textList[index]

  // 为每个字符创建一个包裹元素，并设置适当的延迟
  const characters = textLine?.split('').map((char, index) => {
    // 检查空格并将其转换为一个可视的空格字符（&nbsp;）
    const isSpace = char === ' '
    const style: React.CSSProperties = {
      animationDelay: `${0.1 * index}s`,
      visibility: isSpace ? 'hidden' : 'visible'
    }

    return (
      <span key={index} className={styles.character} style={style}>
        {isSpace ? '\u00A0' : char} {/* 如果是空格字符，则插入 '\u00A0'，否则插入原字符 */}
      </span>
    )
  })

  // 获取 typewriter 元素的引用
  const typewriterRef = useRef<HTMLParagraphElement>(null)

  // 设置 typewriter 元素的 CSS 变量
  useEffect(() => {
    if (typewriterRef.current) {
      const text = typewriterRef.current.textContent
      const textLength = text!.length
      typewriterRef.current.style.setProperty('--typing-steps', textLength.toString()) // 设置动画步数，即字符数
      typewriterRef.current.style.setProperty('--typing-duration', `${animationTime}s`) // 设置动画时长，单位为秒
    }
  }, [textFragments, textLine, animationTime])

  return (
    <p ref={typewriterRef} className={clsx(styles.typewriter, styles.paragraph)}>
      {characters}
    </p>
  )
}

export default Typewriter
