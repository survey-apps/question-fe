export interface TextFragment {
  title?: string
  author?: string
  contents: string[]
}

export interface TypewriterProps {
  animationTime?: number
  textFragments?: TextFragment[]
  randomOrder?: boolean
}
