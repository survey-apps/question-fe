/* eslint-disable @typescript-eslint/no-explicit-any */
export interface LottieAnimationProps {
  className?: string
  title?: string
  animationData: any
  styleProps?: React.CSSProperties
}
