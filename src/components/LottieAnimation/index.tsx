import React from 'react'
import Lottie from 'react-lottie-player'
import type { LottieAnimationProps } from './types'

const LottieAnimation: React.FC<LottieAnimationProps> = ({
  className = '',
  title = '',
  animationData,
  styleProps = {}
}) => {
  const defaultOptions = {
    play: true,
    loop: true,
    autoPlay: true,
    speed: 1,
    name: title
  }

  return (
    <Lottie
      className={className}
      {...defaultOptions}
      animationData={animationData}
      style={{
        width: '150px',
        height: '150px',
        background: 'transparent',
        ...styleProps
      }}
      rendererSettings={{ preserveAspectRatio: 'xMidYMid slice' }}
    />
  )
}

export default LottieAnimation
