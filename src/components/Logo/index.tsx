import React from 'react'
import clsx from 'clsx'
import { Divider } from 'antd'
import styles from './styles.module.scss'
import type { LogoProps } from './types'

const Logo: React.FC<LogoProps> = ({
  className,
  title,
  color,
  SvgIcon,
  svgFill = 'currentColor',
  svgWidth = 24,
  svgHeight = 24,
  showDivider = false
}) => {
  return (
    <div className={clsx(styles.container, className)} style={{ color: color }}>
      <SvgIcon className={styles.logo} fill={svgFill} width={svgWidth} height={svgHeight} />
      <h1 className={styles.title}>{title}</h1>
      {showDivider && <Divider type="vertical" />}
    </div>
  )
}

export default Logo
