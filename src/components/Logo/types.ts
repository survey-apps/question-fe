import React from 'react'

export interface LogoProps {
  className?: string
  title: string
  color?: string
  SvgIcon: React.FunctionComponent<
    React.SVGProps<SVGSVGElement> & {
      title?: string | undefined
    }
  >
  svgFill?: string
  svgWidth?: number
  svgHeight?: number
  showDivider?: boolean
}
