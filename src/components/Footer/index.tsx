import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import clsx from 'clsx'
import styles from './styles.module.scss'
import type { CopyrightProps } from './types'
import { timestampToDateTime, timestampToRuntime } from '@/utils/date'

const badges = [
  {
    name: 'Frame',
    link: 'https://react.dev/',
    logo: 'React',
    color: '#087EA4'
  },
  {
    name: 'UI',
    link: 'https://ant.design/',
    logo: 'Ant Design',
    color: '#1677FF'
  },
  {
    name: 'Build',
    link: 'https://vitejs.dev/',
    logo: 'Vite',
    color: '#F8C517'
  },
  {
    name: 'Hosted',
    link: 'https://vercel.com/',
    logo: 'Vercel',
    color: '#171717'
  },
  {
    name: 'Source',
    link: 'https://gitlab.com/',
    logo: 'Gitlab',
    color: '#FC6D26'
  },
  {
    name: 'License',
    link: 'https://opensource.org/license/mit',
    logo: 'OpenSourceInitiative',
    color: '#3DA639'
  }
]

const BadgeList = () => {
  return badges.map(badge => {
    const { name, link, logo, color } = badge
    const url = `https://img.shields.io/badge/${name}-${logo.replace(' ', '%20')}-${color.replace('#', '')}?style=flat&logo=${logo.replace(' ', '%20')}`
    return (
      <Link key={name} to={link} target="_blank" rel="noreferrer">
        <img src={url} alt={name} />
      </Link>
    )
  })
}

const Footer: React.FC<CopyrightProps> = ({
  className,
  appName,
  author = '',
  createdAt = 1687219200000 // 2023-06-20T08:00:00.000UTC+8
}) => {
  const [runtime, setRuntime] = useState({ year: 0, day: 0, hour: 0, minute: 0, second: 0 })

  useEffect(() => {
    const timer = setInterval(() => {
      const currentRuntime = timestampToRuntime(createdAt)
      setRuntime(currentRuntime)
    }, 1000)

    return () => clearInterval(timer)
  }, [createdAt])

  const { year: CreatedYear } = timestampToDateTime(createdAt)

  return (
    <div className={clsx(styles.container, className)}>
      <p>
        Copyright &copy; {CreatedYear} {appName}
        {author && `, @${author}`}
      </p>
      <p>All rights reserved.</p>
      <div className={styles.runtime}>
        本站运行了 {runtime.year} 年 {runtime.day} 天 {runtime.hour} 小时 {runtime.minute} 分{' '}
        {runtime.second < 10 ? '0' + runtime.second : runtime.second} 秒
      </div>
      <div className={styles.badges}>
        <BadgeList />
      </div>
    </div>
  )
}

export default Footer
