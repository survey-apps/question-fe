export interface CopyrightProps {
  className?: string
  appName: string
  author?: string
  createdAt?: number // timestamp in seconds
}
