interface NavItem {
  label: string
  value: string
}

type NavList = Array<NavItem>

interface NavBarProps {
  className?: string
  navList?: NavList
}

export type { NavList, NavItem, NavBarProps }
