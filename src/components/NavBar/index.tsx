import React from 'react'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import styles from './styles.module.scss'
import type { NavBarProps } from './types'
import navArray from './data.json'

const NavBar: React.FC<NavBarProps> = ({ className, navList = navArray }) => {
  return (
    <div className={clsx(styles.container, className)}>
      <ul className={styles['nav-list']} role="list">
        {navList.map(item => {
          const { label, value } = item
          return (
            <li key={label} className={styles['nav-item']}>
              <Link to={value}>{label}</Link>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default NavBar
