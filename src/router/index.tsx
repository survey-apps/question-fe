import { createBrowserRouter } from 'react-router-dom'

import DefaultLayout from '@/layouts/DefaultLayout'
import QuestionnaireLayout from '@/layouts/QuestionnaireLayout'
import PostLayout from '@/layouts/PostLayout'

import Home from '@/pages/(base)/Home'
import Login from '@/pages/(base)/Login'
import Register from '@/pages/(base)/Register'
import NotFound from '@/pages/(base)/404'

import Lists from '@/pages/(view)/questionnaire/Lists'
import Stars from '@/pages/(view)/questionnaire/Stars'
import Trashes from '@/pages/(view)/questionnaire/Trashes'

import Detail from '@/pages/(view)/post/Detail'
import Edit from '@/pages/(view)/post/Edit'
import Statistic from '@/pages/(view)/post/Statistic'

import {
  ROOT_PATHNAME,
  HOME_PATHNAME,
  LOGIN_PATHNAME,
  REGISTER_PATHNAME,
  QUESTIONNAIRE_PATHNAME,
  QUESTIONNAIRE_LISTS_PATHNAME,
  QUESTIONNAIRE_TRASHES_PATHNAME,
  QUESTIONNAIRE_STARS_PATHNAME,
  POST_PATHNAME,
  POST_EDIT_PATHNAME,
  POST_STATISTIC_PATHNAME,
  NOT_FOUND_PATHNAME
} from './paths'

const router = createBrowserRouter([
  {
    path: ROOT_PATHNAME,
    element: <DefaultLayout />,
    children: [
      {
        path: ROOT_PATHNAME,
        element: <Home />
      },
      {
        path: HOME_PATHNAME,
        element: <Home />
      },
      {
        path: QUESTIONNAIRE_PATHNAME,
        element: <QuestionnaireLayout />,
        children: [
          {
            path: QUESTIONNAIRE_LISTS_PATHNAME,
            element: <Lists />
          },
          {
            path: QUESTIONNAIRE_STARS_PATHNAME,
            element: <Stars />
          },
          {
            path: QUESTIONNAIRE_TRASHES_PATHNAME,
            element: <Trashes />
          }
        ]
      }
    ]
  },
  {
    path: LOGIN_PATHNAME,
    element: <Login />
  },
  {
    path: REGISTER_PATHNAME,
    element: <Register />
  },
  {
    path: POST_PATHNAME,
    element: <PostLayout />,
    children: [
      {
        path: ':id',
        element: <Detail />
      },
      {
        path: `${POST_EDIT_PATHNAME}/:id`,
        element: <Edit />
      },
      {
        path: `${POST_STATISTIC_PATHNAME}/:id`,
        element: <Statistic />
      }
    ]
  },
  {
    path: NOT_FOUND_PATHNAME,
    element: <NotFound />
  }
])

export default router
