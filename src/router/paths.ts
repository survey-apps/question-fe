export const ROOT_PATHNAME = '/'
export const HOME_PATHNAME = '/home'
export const LOGIN_PATHNAME = '/login'
export const REGISTER_PATHNAME = '/register'
export const QUESTIONNAIRE_PATHNAME = '/questionnaire'
export const QUESTIONNAIRE_LISTS_PATHNAME = '/questionnaire/lists'
export const QUESTIONNAIRE_STARS_PATHNAME = '/questionnaire/stars'
export const QUESTIONNAIRE_TRASHES_PATHNAME = '/questionnaire/trashes'
export const POST_PATHNAME = '/post'
export const POST_EDIT_PATHNAME = '/post/edit'
export const POST_STATISTIC_PATHNAME = '/post/statistic'
export const NOT_FOUND_PATHNAME = '*'
