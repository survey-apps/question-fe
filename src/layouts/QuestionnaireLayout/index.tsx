import React from 'react'
import { Outlet, useNavigate, useLocation } from 'react-router-dom'
import { Button, Divider, Space } from 'antd'
import { BarsOutlined, FileTextOutlined, PlusOutlined, StarOutlined } from '@ant-design/icons'
import styles from './styles.module.scss'
import {
  QUESTIONNAIRE_LISTS_PATHNAME,
  QUESTIONNAIRE_STARS_PATHNAME,
  QUESTIONNAIRE_TRASHES_PATHNAME
} from '@/router/paths'

const QuestionnaireLayout: React.FC = () => {
  const navigate = useNavigate()
  const { pathname } = useLocation()

  return (
    <div className={styles.container}>
      <section className={styles.left}>
        <Space direction="vertical">
          <Button type="primary" size="large" icon={<PlusOutlined />} onClick={() => {}}>
            新建问卷
          </Button>
          <Divider style={{ borderTop: 'transparent' }} />

          <Button
            type={pathname.startsWith(QUESTIONNAIRE_LISTS_PATHNAME) ? 'default' : 'text'}
            size="large"
            icon={<BarsOutlined />}
            onClick={() => navigate(QUESTIONNAIRE_LISTS_PATHNAME)}
          >
            问卷列表
          </Button>
          <Button
            type={pathname.startsWith(QUESTIONNAIRE_STARS_PATHNAME) ? 'default' : 'text'}
            size="large"
            icon={<StarOutlined />}
            onClick={() => navigate(QUESTIONNAIRE_STARS_PATHNAME)}
          >
            星标问卷
          </Button>
          <Button
            type={pathname.startsWith(QUESTIONNAIRE_TRASHES_PATHNAME) ? 'default' : 'text'}
            size="large"
            style={{ width: '120px', textAlign: 'left' }}
            icon={<FileTextOutlined />}
            onClick={() => navigate(QUESTIONNAIRE_TRASHES_PATHNAME)}
          >
            回收站
          </Button>
        </Space>
      </section>
      <section className={styles.right}>
        <Outlet />
      </section>
    </div>
  )
}

export default QuestionnaireLayout
