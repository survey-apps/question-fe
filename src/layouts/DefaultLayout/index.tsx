import React from 'react'
import { Outlet } from 'react-router-dom'
import { Layout } from 'antd'
import Header from '@/components/Header'
import Footer from '@/components/Footer'
import styles from './styles.module.scss'
import navList from './config'

const { Footer: AntdFooter, Content: AntdContent } = Layout

const DefaultLayout: React.FC = () => {
  return (
    <Layout>
      <Header className={styles.header} title="My Blog" navList={navList} />

      <AntdContent className={styles.main}>
        <Outlet />
      </AntdContent>

      <AntdFooter className={styles.footer}>
        <Footer appName="Blog Design System" author="Mystic" />
      </AntdFooter>
    </Layout>
  )
}

export default DefaultLayout
