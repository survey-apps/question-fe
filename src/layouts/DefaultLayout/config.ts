const navList = [
  {
    label: '首页',
    value: '/home'
  },
  {
    label: '问卷列表',
    value: '/questionnaire/lists'
  },
  {
    label: '星标问卷',
    value: '/questionnaire/stars'
  },
  {
    label: '回收站',
    value: '/questionnaire/trashes'
  },
  // {
  //   label: '文章列表',
  //   value: '/blog/lists'
  // },
  // {
  //   label: '我的收藏',
  //   value: '/blog/collections'
  // },
  // {
  //   label: '草稿纸',
  //   value: '/blog/drafts'
  // },
  {
    label: '更多...',
    value: '#'
  }
]

export default navList
