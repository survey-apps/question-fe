import { FC } from 'react'
import { Outlet } from 'react-router-dom'

const PostLayout: FC = () => {
  return (
    <div>
      <div>PostLayout</div>
      <Outlet />
    </div>
  )
}

export default PostLayout
