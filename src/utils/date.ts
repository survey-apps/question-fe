interface DateTime {
  year: number
  month?: number
  day: number
  hour: number
  minute: number
  second: number
}

interface DateTimeString {
  year: string
  month: string
  day: string
  hour: string
  minute: string
  second: string
}

/**
 * @description: 将时间戳转换为UTC时间对象
 */
export function timestampToUTCDateTime(timestamp: number): DateTime {
  const creationDate = new Date(timestamp)

  const year = creationDate.getUTCFullYear()
  const month = creationDate.getUTCMonth() + 1
  const day = creationDate.getUTCDate()
  const hour = creationDate.getUTCHours()
  const minute = creationDate.getUTCMinutes()
  const second = creationDate.getUTCSeconds()

  return {
    year: year,
    month: month,
    day: day,
    hour: hour,
    minute: minute,
    second: second
  }
}

/**
 * @description: 将时间戳转换为Local时间对象
 */
export function timestampToLocalDateTime(timestamp: number): DateTime {
  const creationDate = new Date(timestamp)

  const year = creationDate.getFullYear()
  const month = creationDate.getMonth() + 1
  const day = creationDate.getDate()
  const hour = creationDate.getHours()
  const minute = creationDate.getMinutes()
  const second = creationDate.getSeconds()

  return {
    year: year,
    month: month,
    day: day,
    hour: hour,
    minute: minute,
    second: second
  }
}

/**
 * @description: 将时间戳转换为时间对象
 */
export function timestampToDateTime(timestamp: number, isUTC: boolean = false): DateTime {
  if (isUTC) {
    return timestampToUTCDateTime(timestamp)
  }
  return timestampToLocalDateTime(timestamp)
}

/**
 * @description: 将时间戳转换为字符串对象
 */
export function timestampToDateTimeString(
  timestamp: number,
  isUTC: boolean = false
): DateTimeString {
  const { year, month, day, hour, minute, second } = timestampToDateTime(timestamp, isUTC)

  const padZero = (n: number): string => (n < 10 ? `0${n}` : n.toString())

  return {
    year: year.toString(),
    month: padZero(month!),
    day: padZero(day),
    hour: padZero(hour),
    minute: padZero(minute),
    second: padZero(second)
  }
}

/**
 * @description: 将时间戳转换为格式化字符串`YYYY-MM-DD HH:mm:ss`
 */
export function timestampToDateTimeFmtString(timestamp: number, isUTC: boolean = false): string {
  const { year, month, day, hour, minute, second } = timestampToDateTimeString(timestamp, isUTC)

  return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}

/**
 * @description: 将时间戳转换为运行时间
 */
export function timestampToRuntime(pastTimestamp: number, isUTC: boolean = false): DateTime {
  let currentTime

  if (isUTC) {
    currentTime = Date.now()
  } else {
    const offset = new Date().getTimezoneOffset() * 60 * 1000
    currentTime = Date.now() - offset
  }

  if (pastTimestamp > currentTime) {
    throw new Error('Timestamp is in the future!')
  }

  let diff = (currentTime - pastTimestamp) / 1000

  const secondsPerMinute = 60
  const secondsPerHour = secondsPerMinute * 60
  const secondsPerDay = secondsPerHour * 24
  const secondsPerYear = secondsPerDay * 365

  const years = Math.floor(diff / secondsPerYear)
  diff -= years * secondsPerYear
  const days = Math.floor(diff / secondsPerDay)
  diff -= days * secondsPerDay
  const hours = Math.floor(diff / secondsPerHour)
  diff -= hours * secondsPerHour
  const minutes = Math.floor(diff / secondsPerMinute)
  diff -= minutes * secondsPerMinute
  const seconds = Math.floor(diff)

  return {
    year: years,
    day: days,
    hour: hours,
    minute: minutes,
    second: seconds
  }
}
