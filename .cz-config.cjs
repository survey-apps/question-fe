module.exports = {
  types: [
    { value: 'feat', name: 'feat ✨:    Introduce new features.' },
    { value: 'fix', name: 'fix 🐛:    Fix a bug.' },
    { value: 'docs', name: 'docs 📝:    Add or update documentation.' },
    { value: 'style', name: 'style 💄:    Add or update the UI and style files.' },
    { value: 'refactor', name: 'refactor ♻️:    Code refactoring.' },
    { value: 'perf', name: 'perf ⚡️:    Improve performance.' },
    { value: 'test', name: 'test ✅:    Add, update, or pass tests.' },
    { value: 'chore', name: 'chore 🧹:    Update build process or tools.' },
    { value: 'revert', name: 'revert ⏪:    Revert changes.' },
    { value: 'tool', name: 'tool 🔧:    Add or update dev configuration files.' },
    { value: 'build', name: 'build 📦:    Compile or package changes.' },
    { value: 'update', name: 'update 🚀:    Dependency upgrades.' }
  ],
  scopes: [
    { name: 'Components' },
    { name: 'Styles' },
    { name: 'Documentation' },
    { name: 'Other' }
  ],
  allowTicketNumber: false,
  isTicketNumberRequired: false,
  ticketNumberPrefix: 'TICKET-',
  ticketNumberRegExp: '\\d{1,5}',
  messages: {
    type: "What's the type of your change?",
    scope: 'Select a scope (optional):',
    customScope: 'Denote the SCOPE of this change:',
    subject: 'Write a short description:\n',
    body: 'Provide a longer description, use "|" to start a new line (optional):\n',
    breaking: 'Note any breaking changes (optional):\n',
    footer: 'List any issues closed by this change (optional):\n',
    confirmCommit: 'Are you sure you want to proceed with the above commit?'
  },
  allowCustomScopes: true,
  allowBreakingChanges: ['feat', 'fix'],
  skipQuestions: ['body', 'footer'],
  breaklineChar: '|',
  footerPrefix: 'ISSUES CLOSED:',
  subjectLimit: 100
}
