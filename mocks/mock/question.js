const Mock = require('mockjs')

const Random = Mock.Random

module.exports = [
  {
    url: '/api/question/:id',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'success',
        data: {
          id: Random.guid(),
          title: Random.ctitle(5, 20),
          content: Random.cparagraph(5, 20)
        }
      }
    }
  },
  {
    url: '/api/question/:id',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'success',
        data: {
          id: Random.id()
        }
      }
    }
  }
]
