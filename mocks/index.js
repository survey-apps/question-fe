const Koa = require('koa')
const Router = require('koa-router')
const mockList = require('./mock/index')

const app = new Koa()
const router = new Router()

// 模拟延迟
async function delay(fn) {
  return new Promise(resolve => {
    setTimeout(() => {
      const res = fn()
      resolve(res)
    }, 500)
  })
}

// 注册 mock 路由
mockList.forEach(item => {
  const { method, url, response } = item
  router[method](url, async ctx => {
    const res = await delay(response)
    ctx.body = res
  })
})

// 使用路由
app.use(router.routes())

// 监听端口
app.listen(3001)
